# Contact

Team FSC nodigt iedereen van harte uit om feedback te geven, vragen te stellen en mee te helpen de standaard zo relevant en toepasbaar mogelijk te maken. We beloven niet dat we met alle feedback iets doen, maar beloven wel dat alle feedback transparant besproken wordt en de conclusies worden teruggekoppeld.
