### Sprint review

Iedere oneven week is op maandag van 10:00 tot 11:00 de sprint review van het team. Samen met de aanwezige organisaties bespreken we de voortgang, identificeren we nieuw werk, kan iedereen vragen stellen en feedback geven. We bespreken ook de roadmap. De sprint review is altijd dezelfde [online Teams meeting](https://teams.microsoft.com/l/meetup-join/19%3ameeting_NjViYTM0YjEtMDZhOS00YzI3LTliZmYtYzMzZTNhZTc0YWZm%40thread.v2/0?context=%7b%22Tid%22%3a%226ef029ab-3fd7-4d98-9b0e-d1f5fedea6d1%22%2c%22Oid%22%3a%222792491b-bf61-4388-a055-8272a0083768%22%7d).

### Mail

Via mail is het team bereikbaar via [edward.vangelderen@vng.nl](mailto:edward.vangelderen@vng.nl).

### Slack

Team FSC gebruikt ook Slack om met de community in contact te staan. Wanneer u nog geen lid bent, kunt u [toegang aanvragen](https://join.slack.com/t/samenorganiseren/shared_invite/zt-258vvmbgl-~Puj_q8_ugsEsMPe9quiEA) tot de community op Slack. Daarna moet u dan nog het FSC kanaal kiezen, waarna u toegang heeft tot een levendige community. 

### GitLab

De [standaard en testsuite](https://commonground.gitlab.io/standards/fsc/) zijn te vinden op GitLab.
De [broncode](https://gitlab.com/commonground/nlx/fsc-nlx) van de FSC implementatie staat, net als de documentatie, ook op GitLab.
