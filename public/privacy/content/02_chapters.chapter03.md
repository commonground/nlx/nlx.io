---
collapsibles:
  - title: Grondslag
    ariaLabel: Grondslag
    body: VNG Realisatie BV verwerkt deze persoonsgegevens op basis van gerechtvaardigd belang.
  - title: Categorieën Persoonsgegevens
    ariaLabel: Categorieën Persoonsgegevens
    body: <li>Naam</li><li>Functie</li><li>Organisatie</li><li>E-mailadres</li>
    element: ul
  - title: Geen ontvangers, geen doorgifte, geen profiele geen geautomatiseerde besluitvorming
    ariaLabel: Ontvangers van persoonsgegevens
    body: Bij NLX.io is geen sprake van ontvangers van persoonsgegevens. De persoonsgegevens van NLX.io worden niet buiten de Europese Economische Ruimte (EER) verwerkt. Er worden geen profielen opgesteld. Er vindt geen geautomatiseerde besluitvorming plaats.
  - title: Bewaartermijnen
    ariaLabel: Bewaartermijnen
    body: De gegevens worden bewaard zolang noodzakelijk voor het doel van de verwerking.
---

## 3. Verwerken van persoonsgegevens

Hieronder wordt meer uitleg gegeven over de verwerking, inclusief het doel, de grondslag en andere belangrijke informatie.

### Doel Componentencatalogus

Het doel van het verwerken van persoonsgegevens voor de Componentencatalogus: Het verstrekken van contactgegevens bij producten in de Componentencatalogus. De Componentencatalogus biedt een overzicht (catalogus) van herbruikbare software binnen Nederlandse gemeenten. Hiermee wordt zichtbaar welke componenten herbruikbaar zijn, en wordt het makkelijker om software daadwerkelijk te hergebruiken.
