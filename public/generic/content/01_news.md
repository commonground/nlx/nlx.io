---
imageLink: generic/content/news.svg
imageAlt: A person giving a presentation on flipboard
newsLink1Text: FSC agenda
newsLink1Href: https://commonground.nl/groups/view/7edd07a0-1f96-4bba-967f-ac72347f63ef/team-core-components/events
newsLink2Text: FSC presentaties
newsLink2Href: https://commonground.nl/groups/view/7edd07a0-1f96-4bba-967f-ac72347f63ef/team-core-components/files
---

## Nieuws en ontwikkelingen

Op de hoogte blijven van nieuws en ontwikkelingen rondom FSC? Regelmatig worden er (online) technische kennissessies georganiseerd. Iedere twee weken is de sprint review waarin de voortgang van het scrum team besproken wordt. Onder andere de sprint review presentaties zijn via de onderstaande link te vinden. 
