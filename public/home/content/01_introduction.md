# Van NLX naar Federatieve Service Connectiviteit (FSC)

De relatie tussen NLX en FSC is niet voor iedereen duidelijk. Op deze pagina wordt uitgelegd hoe NLX is ontstaan en hoe FSC daaruit is voortgekomen.  
De korte versie is: NLX beoogde verplichte software te zijn voor uniforme connectiviteit als enabler voor data bij de bron. 
Na een Gartner onderzoek bleek dat uniforme connectiviteit daar randvoorwaardelijk voor is, maar niet via verplichte software. 
Daarop is het NLX pad verlaten en is FSC als standaard geboren.
