## Onderdeel van Common Ground

Gemeenten hebben een nieuwe, moderne, gezamenlijke informatievoorziening nodig voor het uitwisselen van gegevens. Want het huidige stelsel voor gegevensuitwisseling maakt het lastig om snel en flexibel te vernieuwen, te voldoen aan privacywetgeving en efficiënt om te gaan met data. Dat staat de verbetering van de gemeentelijke dienstverlening in de weg.

Vanuit die behoefte is Common Ground ontstaan. In de kern gaat het bij Common Ground hierom: een hervorming van de gemeentelijke informatievoorziening, door op een andere manier om te gaan met gegevens. Zo koppelen we data los van werkprocessen en applicaties. En we bevragen data bij de bron, in plaats van ze veelvuldig te kopiëren en op te slaan. Met de herinrichting van de informatievoorziening kunnen gemeenten hun dienstverlening en bedrijfsvoering ingrijpend verbeteren. Dat stelt ze in staat om op een moderne en flexibele manier in te spelen op maatschappelijke opgaven. Inmiddels is dat verankerd in de [informatiekundige visie Common Ground](https://www.gemmaonline.nl/index.php/Common_Ground).

## 2018 - NLX als verplichte software

NLX, als landelijke integratie faciliteit (feitelijk gaat het over het connectiviteitsdeel – integratie is meer dan connectiviteit) is vanaf het prille begin integraal onderdeel van Common Ground geweest. Het is een van de functies in de derde laag in het Common Ground vijflagenmodel waarmee laagdrempelig toepassing en data gescheiden kan worden. Een oplossing als NLX, die gegevens van (overheids)organisaties verbindt, is nog niet beschikbaar. Geïnspireerd door het bewezen succes van de Estlandse X-Road besloot een aantal gemeenten om onderzoek te doen naar een X-Road voor Nederland. Vandaar NLX.

NLX ontwikkelde zich de afgelopen jaren bij VNG Realisatie door het in te zetten als validatie middel voor experimenten rondom uniforme connectiviteit. We richtten ons op de strategie van verplichte software voor gemeenten, haar ketenpartners en haar leveranciers. Het idee achter verplichte software was dat iedereen gemakkelijk bij zou kunnen dragen aan deze software. Hoe minder verschillende implementaties, hoe minder complex het landschap is en dus hoe wendbaarder (dit deel van) het landschap is. Hoewel veel verschillende organisaties beproevingen succesvol uitgevoerd hebben, bleef de echte adoptie in productie uit.

## 2022 - Objectiveringsonderzoek door Gartner

In 2022 heeft VNG Realisatie een objectiveringsonderzoek over NLX laten uitvoeren door Gartner. De onderzoeksvragen waren:
- In hoeverre zijn een uitwissel- en integratiemechanisme benodigd voor de realisatie van de visie Common Ground? In welke mate is er behoefte bij gemeenten en andere overheidsinstellingen voor de voorgestelde standaardisatie?
- Hoe kan de benodigde software het best beschikbaar worden gesteld voor succesvolle implementatie en kan er worden aangesloten op open standaarden en/of software oplossingen die in de markt beschikbaar zijn?
- Wat zijn de implicaties voor succesvolle implementatie en welke handvatten zijn hierbij van belang?

Dit onderzoek leverde waardevolle inzichten op over waarom de adoptie van NLX achterbleef en heeft richting gegeven aan het connectiviteitsvraagstuk van Common Ground. De belangrijkste conclusies waren:
- NLX als standaard is nodig; NLX als software is enkel nodig voor de API Directory en de test suite als onderdeel van de NLX standaard. Waar nog als sub conclusie genoemd wordt dat de beschikbaarheid van de referentie-implemenatie (waarmee de NLX software bedoeld werd) een versnellend effect zal hebben op de adoptie van de standaard;
- Communicatie over de NLX standaard moet worden toegesneden op de behoeften van de verschillende stakeholdergroepen;
- Om de standaard bruikbaar te maken, moeten generieke en overheidsstandaarden verder gecontextualiseerd worden voor het Common Ground ecosysteem;

In het volledige [rapport](https://commonground.nl/file/download/6fef7d23-64d2-45f4-9ee8-403168e53897/gartner-objectivering-nlx-v1.pdf) is de redenatie terug te lezen samen met nog meer (deel) conclusies en aanbevelingen.

## 2023 - FSC als standaard gebaseerd op de lessen van NLX en de aanbevelingen van Gartner

In de lijn van de conclusies van Gartner is het pad van verplichte software verlaten. Alle ervaringen en lessen van de afgelopen jaren zijn opgeschreven in de vorm van de [FSC standaard](https://commonground.gitlab.io/standards/fsc/). FSC staat voor Federatieve Service Connectiviteit. We hebben conceptuele verbeteringen doorgevoerd die we op onze roadmap hadden staan, een eerste versie van een testsuite gemaakt en een nieuwe implementatie gebouwd. [De implementatie](https://docs.fsc.nlx.io/introduction) heeft voor ons in volgorde drie doelen:

1. Validatiemiddel van ideeën; 
2. Demo waarmee zichtbaar aangetoond wordt dat de standaard werkt; 
3. Een hulpmiddel om na vaststelling van de standaard gemakkelijker van start te gaan (versnelling van de adoptie);

De standaard kan door iedere organisatie geïmplementeerd worden naar eigen behoefte. Vanuit de consultatie en de feedback tijdens de sprint reviews zijn er drie implementatie strategieën te zien:
1. Een volledige eigen implementatie;
2. Een hybride implementatie waarbij er een combinatie gemaakt wordt met het Manager component van de FSC implementatie samen met de (commerciële) gateway die al beschikbaar is;
3. Een start maken door het inzetten van de volledige FSC implementatie om vervolgens te besluiten hoe later verder te gaan;


## Standaardisatie trajecten

Potentieel lopen twee standaardisatie trajecten parallel aan elkaar: een gemeentelijke standaardisering en een landelijke standaardisering. De FSC standaard, testsuite en implementatie is via het Common Ground programma ter consultatie aangeboden. De resultaten daaruit worden besproken in het Common Ground board, waarna advies gegeven wordt aan het MT van VNG Realisatie. Afhankelijk van dat advies en de visie van het MT wordt besloten om het gemeentelijk standaardisatie proces te doorlopen. Dit besluit zal mede afhangen van de stappen die op het landelijke standaardisatie proces gezet wordt. Op verzoek van BZK wordt samen met Logius en Stichting RINIS door VNG Realisatie een wijzigingsvoorstel voorbereid op de Digikoppeling voor API’s standaard bij Logius. Naast dit wijzigingsvoorstel worden samen met Stichting RINIS landelijke beproevingen georganiseerd om FSC ook op landelijk niveau goed uit te leggen en ervaring mee op te doen. Dit proces moet in 2024 leiden tot het optillen van de FSC standaard in de Digikoppeling voor API’s standaard.
