// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'
import Head from 'next/head'
import Header from 'src/components/Header'
import News from 'src/components/NewsSection'
import Footer from 'src/components/Footer'
import Introduction from './Introduction'
import Background from './Background'
import Commonground from './Commonground'
import AchterliggendeStandaarden from './AchterliggendeStandaarden'

const Home = ({ sections }) => (
  <div>
    <Head>
      <title>NLX - Homepage</title>
      <meta name="description" content={sections.meta.description} />
    </Head>

    <Header />

    <main>
      <Introduction {...sections.introduction} />
      <Background {...sections.background} />
      <Commonground {...sections.commonground} />
      <AchterliggendeStandaarden {...sections.achterliggendestandaarden} />
      <News {...sections.news} />
    </main>

    <Footer />
  </div>
)

Home.propTypes = {
  sections: object.isRequired,
}

export default Home
