// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import BaseSection from 'src/components/Section'
import { Col } from 'src/components/Grid'

export const Section = styled(BaseSection)`
  &::before {
    border-top-color: ${(p) => p.theme.colorAverageBlue};
  }
`

export const ImageCol = styled(Col)`
  display: flex;
  justify-content: space-around;
  align-items: center;
  order: -1;
  padding-top: 3rem;

  ${mediaQueries.smUp`
    order: 0;
  `}
`

export const Image = styled.img`
  object-fit: cover;
  width: 100%;
  max-width: 112px;

  ${mediaQueries.smUp`
    max-width: 200px;
  `}
`
