// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import BaseSection from 'src/components/Section'

export const Section = styled(BaseSection)`
  padding-top: 0;
`
