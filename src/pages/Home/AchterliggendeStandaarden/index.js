// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import { Container } from 'src/components/Grid'
import HtmlContent from 'src/components/HtmlContent'
import { Section } from './index.styles'

const AchterliggendeStandaarden = ({ content }) => (
  <Section omitArrow>
    <Container>
      <HtmlContent content={content} />
    </Container>
  </Section>
)

AchterliggendeStandaarden.propTypes = {
  content: string,
}

export default AchterliggendeStandaarden
