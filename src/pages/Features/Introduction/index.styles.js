// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import BaseSection, { SectionIntro } from 'src/components/Section'

export const Section = styled(BaseSection)`
  padding: ${(p) => p.theme.tokens.spacing09} 0;
  background-color: ${(p) => p.theme.colorAverageBlue};
  background-image: url('features/intro-bg-small.svg');

  ${mediaQueries.mdUp`
    padding: ${(p) => p.theme.tokens.spacing10} 0;
    background: url('features/intro-bg-large.svg') no-repeat center bottom,
      ${(p) => p.theme.gradientBlue};
  `}
`

export const StyledSectionIntro = styled(SectionIntro)`
  color: ${(p) => p.theme.tokens.colorBackground};
`
