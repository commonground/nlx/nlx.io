// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { object } from 'prop-types'
import Head from 'next/head'
import Header from 'src/components/Header'
import News from 'src/components/NewsSection'
import Footer from 'src/components/Footer'
import Introduction from './Introduction'
import Feature from './Feature'

const About = ({ sections }) => (
  <div>
    <Head>
      <title>NLX - Features</title>
      <meta name="description" content={sections.meta.description} />
    </Head>

    <Header />

    <main>
      <Introduction {...sections.introduction} />
      <Feature featureName="auditing" {...sections.auditing} />
      <Feature
        featureName="access"
        {...sections.access}
        wideImagePosition="left"
      />
      <Feature featureName="management_ui" {...sections.management_ui} />
      <Feature featureName="directory" {...sections.directory} />
      <Feature featureName="environments" {...sections.environments} />
      <Feature
        featureName="terms"
        {...sections.terms}
        wideImagePosition="left"
        narrowImage
      />
      <News {...sections.news} />
    </main>

    <Footer />
  </div>
)

About.propTypes = {
  sections: object.isRequired,
}

export default About
