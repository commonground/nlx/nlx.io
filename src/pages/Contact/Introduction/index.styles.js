// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import BaseSection from 'src/components/Section'

export const Section = styled(BaseSection)`
  background: #dfe5ea url('contact/intro-bg-small.svg') no-repeat center bottom;

  ${mediaQueries.mdUp`
    padding: ${(p) =>
      `${p.theme.tokens.spacing09} 0 ${p.theme.tokens.spacing12}`};
    background: url('contact/intro-bg-large.svg') no-repeat center bottom,
      linear-gradient(to right, #e6e9ed, #cdd6e3);
  `}
`
