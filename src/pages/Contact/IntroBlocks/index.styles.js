// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import Icon from '@commonground/design-system/dist/components/Icon'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import BaseSection from 'src/components/Section'
import { IconExternalLink as ExternalLink } from 'src/icons'

export const Section = styled(BaseSection)`
  padding-bottom: ${(p) => p.theme.tokens.spacing09};

  ${mediaQueries.mdUp`
    padding: 0;
  `}
`

export const Block = styled.div`
  ${mediaQueries.mdUp`
    height: 100%;
    padding: ${(p) =>
      `${p.theme.tokens.spacing07} ${p.theme.tokens.spacing07} 0 ${p.theme.tokens.spacing07}`};
    background-color: ${(p) => p.theme.tokens.colorBackground};
    transform: translateY(-${(p) => p.theme.tokens.spacing09});

    h2 {
      margin-top: 0;
    }
  `}
`

export const List = styled.ul`
  padding: 0;
  list-style-type: none;
`

export const Item = styled.li`
  margin: 0 0 ${(p) => p.theme.tokens.spacing03} 0;
`

export const StyledIcon = styled(Icon)`
  fill: ${(p) => p.theme.tokens.colorPaletteGray700};
`

export const IconExternalLink = styled(ExternalLink)`
  width: ${(p) => p.theme.tokens.iconSizeSmall};
  height: ${(p) => p.theme.tokens.iconSizeSmall};
  margin-left: ${(p) => p.theme.tokens.spacing03};
  fill: ${(p) => p.theme.tokens.colorPaletteGray600};
  transform: translateY(1px);
`
