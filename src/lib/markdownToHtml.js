// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { remark } from 'remark'
import html from 'remark-html'
import headingId from 'remark-heading-id'

export default async function markdownToHtml(markdown) {
  const result = await remark().use(headingId).use(html).process(markdown)
  return result.toString()
}
