// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { string } from 'prop-types'
import Link from 'next/link'
import Button from '@commonground/design-system/dist/components/Button'
import { Icon, IconExternalLink } from './index.styles'

const LinkButton = ({ href, text, ...props }) => {
  const isExternal = href && href.substring(0, 4) === 'http'
  const rel = isExternal ? { rel: 'noreferrer' } : {}

  return text && href ? (
    <Link href={href} passHref>
      <Button as="a" variant="link" {...rel} {...props}>
        {text}
        {isExternal && <Icon as={IconExternalLink} inline />}
      </Button>
    </Link>
  ) : null
}

LinkButton.propTypes = {
  href: string,
  text: string,
}

export default LinkButton
