// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import { bool } from 'prop-types'
import PrimaryNavigation from '@commonground/design-system/dist/components/PrimaryNavigation'
import { useRouter } from 'next/router'
import { Container } from '../Grid'
import NavLink from '../NavLink'
import { LogoWrapper, StyledNLXLogo, NavigationWrapper } from './index.styles'

const Header = ({ homepage }) => {
  const { pathname } = useRouter()

  return (
    <>
      <LogoWrapper homepage={homepage}>
        <Container>
          <StyledNLXLogo />
        </Container>
      </LogoWrapper>

      <NavigationWrapper>
        <PrimaryNavigation
          LinkComponent={NavLink}
          pathname={pathname}
          mobileMoreText="Meer"
          items={[
            {
              name: 'Home',
              to: '/',
            },
            {
              name: 'Contact',
              to: '/contact',
            },
            {
              name: 'FSC Docs',
              to: 'https://docs.fsc.nlx.io/',
              target: '_blank',
            },
            {
              name: 'Directory',
              to: 'https://directory-ui.demo.fsc.nlx.io/',
              target: '_blank',
            },
          ]}
        />
      </NavigationWrapper>
    </>
  )
}

Header.propTypes = {
  homepage: bool,
}

Header.defaultProps = {
  homepage: false,
}

export default Header
