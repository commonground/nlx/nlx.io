// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import styled from 'styled-components'
import mediaQueries from '@commonground/design-system/dist/mediaQueries'
import { mobileNavigationHeight } from '@commonground/design-system/dist/components/PrimaryNavigation'
import LogoVng from './vng.svg'

export const Wrapper = styled.footer`
  padding: ${(p) => p.theme.tokens.spacing07} 0;
  background: #154967 url('generic/footer-bg-small.svg') no-repeat center bottom;

  ${mediaQueries.smDown`
    margin-bottom: ${mobileNavigationHeight};
  `}
`

export const FooterContent = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  text-align: left;

  > a:not(:last-child) {
    align-items: center;
    display: flex;
    color: ${(p) => p.theme.tokens.colors.colorPaletteGray200};
  }
`

export const List = styled.ul`
  display: flex;
  flex-direction: column;
  padding: 0;
  margin: 0;
  list-style-type: none;

  ${mediaQueries.smUp`
    flex-direction: row;
  `}
`

export const StyledLogoVng = styled(LogoVng)`
  width: 100px;
`
