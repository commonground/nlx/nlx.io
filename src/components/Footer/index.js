// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import React from 'react'
import Link from 'next/link'

import { Container } from 'src/components/Grid'
import { FooterContent, Wrapper, StyledLogoVng } from './index.styles'

const Footer = () => (
  <Wrapper>
    <Container>
      <FooterContent>
        <Link href="/privacyverklaring">
          <a>Privacyverklaring</a>
        </Link>
        <StyledLogoVng />
      </FooterContent>
    </Container>
  </Wrapper>
)

export default Footer
