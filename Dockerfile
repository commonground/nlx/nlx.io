FROM node:19.3.0-alpine AS build

# Install required build tools
RUN apk add --no-cache \
    build-base \
    python3 \
    openssl \
    make \
    git

# Copy only package.json & package-lock.json to make the dependency fetching step optional
COPY package.json \
     package-lock.json \
     .babelrc.js \
     jsconfig.json \
    /app/

COPY pages /app/pages/
COPY public /app/public/
COPY src /app/src/

WORKDIR /app

RUN npm install && \
    npm run build

RUN mkdir -p /tmpdir/client_temp \
    /tmpdir/fastcgi_temp \
    /tmpdir/proxy_temp \
    /tmpdir/scgi_temp \
    /tmpdir/uwsgi_temp && \
    touch /tmpdir/nginx.pid && \
    chmod 777 /tmpdir/nginx.pid

# Copy static docs to non privileged nginx scratch container.
FROM ricardbejarano/nginx:1.27.1@sha256:4552e694ecabc0f7e1994d181ffa6e04c8292e49ec649f1deea9360dce97cd22

COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx.conf /etc/nginx/nginx.conf
COPY docker/mime.types /etc/nginx/mime.types

COPY --from=build /app/out /usr/share/nginx/html
COPY --from=build --chown=10000:10000 /tmpdir /tmp
