// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { ThemeProvider } from 'styled-components'
import { elementType, object } from 'prop-types'
import Head from 'next/head'
import GlobalStyles from '@commonground/design-system/dist/components/GlobalStyles'
import DomainNavigation from '@commonground/design-system/dist/components/DomainNavigation'
import '@fontsource/source-sans-pro/latin.css'
import theme from 'src/styling/theme'
import { MediaProvider } from 'src/styling/media'
import '../src/styling/fonts.css'

function MyApp({ Component, pageProps }) {
  return (
    <ThemeProvider theme={theme}>
      <MediaProvider>
        <Head>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>

        {pageProps.statusCode !== 404 && (
          <DomainNavigation
            activeDomain="NLX"
            gitLabLink={'https://gitlab.com/commonground/nlx/fsc-nlx'}
          />
        )}
        <GlobalStyles />
        <Component {...pageProps} />
      </MediaProvider>
    </ThemeProvider>
  )
}

MyApp.propTypes = {
  Component: elementType,
  pageProps: object,
}

export default MyApp
