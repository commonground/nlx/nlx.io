// Copyright © VNG Realisatie 2021
// Licensed under the EUPL
//
import { getPageSections } from 'src/lib/api'
import Privacy from 'src/pages/Privacy'

export default function PrivacyPage(props) {
  return <Privacy {...props} />
}

export async function getStaticProps() {
  const sections = await getPageSections('privacy')
  return { props: { sections } }
}
